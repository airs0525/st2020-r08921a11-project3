import os  

# [Content] XML Footer Text
def test_hasHomePageNode():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
    os.system('adb shell input tap 100 100')

# 1. [Content] Side Bar Text
def test_tapSidebarxml():
    os.system('adb shell input tap 100 100')
    os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('查看商品分類') != -1
    assert xmlString.find('查訂單/退訂退款') != -1
    assert xmlString.find('追蹤/買過/看過清單') != -1
    assert xmlString.find('智慧標籤') != -1
    
# 2. [Screenshot] Side Bar Text
def test_tapSideScreenshot():
    os.system('adb shell input tap 100 100')
    os.system('adb shell screencap -p /sdcard/screen.png')
    os.system('adb pull /sdcard/screen.png .')
    os.system('adb shell input keyevent 4')

# 3. [Context] Categories
def test_Categories():
    os.system('adb shell input swipe 300 1500 300 200')
    os.system('sleep 2')
    os.system('adb shell input tap 992 282')
    os.system('sleep 2')
    os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('精選') != -1
    assert xmlString.find('家電') != -1
    assert xmlString.find('食品') != -1
    assert xmlString.find('日用') != -1
    os.system('adb shell input tap 640 1250')
    os.system('sleep 2')
    
# 4. [Screenshot] Categories
def test_CategoriesScreenshot():
    os.system('adb shell input tap 992 282')
    os.system('sleep 2')
    os.system('adb shell screencap -p /sdcard/Categories.png')
    os.system('adb pull /sdcard/Categories.png .')
    os.system('adb shell input tap 640 1250')
    os.system('sleep 2')
    

# 5. [Context] Categories page
def test_CategoriesPage():
    os.system('adb shell input tap 100 100')
    os.system('sleep 2')
    os.system('adb shell input tap 150 600')
    os.system('sleep 2')
    os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
    os.system('sleep 2')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('24H購物') != -1
    assert xmlString.find('購物中心') != -1
    os.system('adb shell input keyevent 4')
    os.system('sleep 3')

# 6. [Screenshot] Categories page
def test_CategoriesPageScreenshot():
    os.system('adb shell input tap 100 100')
    os.system('sleep 5')
    os.system('adb shell input tap 150 600')
    os.system('sleep 2')
    os.system('adb shell screencap -p /sdcard/CategoriesPage.png')
    os.system('sleep 2')
    os.system('adb pull /sdcard/CategoriesPage.png .')
    
# 7. [Behavior] Search item “switch”
def test_SearchItem():
    os.system('adb shell input tap 426 127')
    os.system('sleep 2')
    os.system('adb shell input text "switch"')
    os.system('sleep 2')
    os.system('adb shell input keyevent 66')
    os.system('sleep 3')
    
# 8. [Behavior] Follow an item and it should be add to the list
def test_FollowAndAddItem():
    os.system('adb shell input tap 600 1050')
    os.system('sleep 5')
    os.system('adb shell input tap 100 1716')
    os.system('sleep 3')
    os.system('adb shell input keyevent 4')
    os.system('sleep 3')
    os.system('adb shell input keyevent 4')
    os.system('sleep 3')
    os.system('adb shell input keyevent 4')
    os.system('sleep 3')
    os.system('adb shell input tap 100 100')
    os.system('sleep 5')
    os.system('adb shell input tap 258 849')
    os.system('sleep 5')
    
    
# 9. [Behavior] Navigate tto the detail of item
def testDetailOdItem():
    os.system('adb shell input tap 241 766')
    os.system('sleep 4')
    os.system('adb shell input swipe 500 1469 500 331')
    os.system('sleep 3')
    os.system('adb shell input tap 549 145')
    os.system('sleep 3')

# 10. [Screenshot] Disconnetion Screen
def testWifiDisconnection():
    os.system('adb shell svc wifi disable')
    os.system('sleep 2')
    os.system('adb shell svc data disable')
    os.system('sleep 2')
    os.system('adb shell screencap -p /sdcard/Disconnection.png')
    os.system('sleep 2')
    os.system('adb pull /sdcard/Disconnection.png .')
